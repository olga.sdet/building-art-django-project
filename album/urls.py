from django.urls import path
from .views import HomePageView, ImagePageView, AboutPageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('image/<int:pk>/', ImagePageView.as_view(), name='image'),
    path('about/', AboutPageView.as_view(), name='about'),
]
