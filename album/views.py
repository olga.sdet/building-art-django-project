from django.views.generic import ListView, DetailView, TemplateView

from .models import Image


class HomePageView(ListView):
    model = Image
    template_name = 'home.html'


class ImagePageView(DetailView):
    model = Image
    template_name = 'image.html'


class AboutPageView(TemplateView):
    template_name = 'about.html'
