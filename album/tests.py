from django.test import TestCase
from django.urls import reverse
from faker import Faker
from http import HTTPStatus

from .models import Image

fake = Faker()


class AlbumTests(TestCase):
    TITLE = fake.name()
    IMAGE_URL = fake.image_url()
    ADDRESS = fake.address()
    ADDRESS_URL = fake.url()

    def setUp(self):

        self.image = Image.objects.create(
            title=self.TITLE,
            image_link=self.IMAGE_URL,
            address=self.ADDRESS,
            address_link=self.ADDRESS_URL,
        )

    def test_string_presentation(self):
        title = fake.name()
        image = Image(title=title)
        self.assertEqual(str(image), image.title)

    def test_image_content(self):
        self.assertEqual(f'{self.image.title}', self.TITLE)
        self.assertEqual(f'{self.image.image_link}', self.IMAGE_URL)
        self.assertEqual(f'{self.image.address}', self.ADDRESS)
        self.assertEqual(f'{self.image.address_link}', self.ADDRESS_URL)

    def test_image_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, self.IMAGE_URL)
        self.assertTemplateUsed(response, 'home.html')

    def test_image_detail_view(self):
        response = self.client.get(f'/image/{self.image.pk}/')
        no_response = self.client.get(f'/image/0/')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(no_response.status_code, HTTPStatus.NOT_FOUND)
        self.assertContains(response, self.IMAGE_URL)
        self.assertContains(response, self.ADDRESS)
        self.assertContains(response, self.ADDRESS_URL)
        self.assertTemplateUsed(response, 'image.html')
